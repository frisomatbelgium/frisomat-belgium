At Frisomat we design, develop, produce and construct buildings made of prefabricated cold-formed steel. From R&D and design over manufacturing to transport, assembly and after-sales. Our cold-formed steel prefab constructions consume 30% less steel, yet are robust and sustainable.

Address: Stokerijstraat 79, 2110 Wijnegem, Belgium

Phone: +32 3 353 33 99
